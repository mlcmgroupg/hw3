clear all, close all, clc;

params = [10; 8/3; 28];     % alpha, beta, rho
dt = 0.01;
tspan = dt:dt:1000;
options = odeset('RelTol', 1e-12, 'AbsTol', 1e-12*ones(1,3));

% rho = 28, starting from (10, 10, 10);
x0 = [10; 10; 10];          % init condition
[t, x1] = ode45(@(t,x) lorenz(t, x, params), tspan, x0, options);
figure;
plot3(x1(:, 1), x1(:, 2), x1(:, 3), 'k', 'LineWidth', 1.5);
title("Lorenz attractor (starting at 10, 10, 10)");

% rho = 28, starting from (10 + 10e-8, 10, 10)
x0 = [10 + 10e-8; 10; 10];          % init condition
[t, x2] = ode45(@(t,x) lorenz(t, x, params), tspan, x0, options);
figure;
plot3(x2(:, 1), x2(:, 2), x2(:, 3), 'k', 'LineWidth', 1.5);
title("Lorenz attractor (starting at 10 + 10e-8, 10, 10)");

% get timestep where error > 1
time = error_diff(x1, x2);
disp("Timestep where error > 1: " + time);

% final difference in trajectory points
final_error = sqrt(sum((x1(length(x1), :) - x2(length(x2), :)) .^ 2));
disp("Final error: " + final_error);

% Changing rho to 0.5
params = [10; 8/3; 0.5];     % alpha, beta, ro

% rho = 0.5 starting from (10, 10, 10)
x0 = [10; 10; 10];          % init condition
[t, x3] = ode45(@(t,x) lorenz(t, x, params), tspan, x0, options);
figure;
plot3(x3(:, 1), x3(:, 2), x3(:, 3), 'k', 'LineWidth', 1.5);
title("Lorenz attractor with ro=0.5 (starting at 10, 10, 10)");

% rho = 0.5 starting from (10 + 10e-8, 10, 10)
x0 = [10 + 10e-8; 10; 10];          % init condition
[t, x4] = ode45(@(t,x) lorenz(t, x, params), tspan, x0, options);
figure;
plot3(x4(:, 1), x4(:, 2), x4(:, 3), 'k', 'LineWidth', 1.5);
title("Lorenz attractor with ro=0.5 (starting at 10 + 10e-8, 10, 10)");

% get timestep where error > 1, if equals length of x3 then no error > 1
ind = error_diff(x3, x4);
disp("Timestep where error > 1: " + ind);

% Changing rho to 1.0
params = [10; 8/3; 8];     % alpha, beta, ro

% rho = 0.5 starting from (10, 10, 10)
x0 = [10; 10; 10];          % init condition
[t, x3] = ode45(@(t,x) lorenz(t, x, params), tspan, x0, options);
figure;
plot3(x3(:, 1), x3(:, 2), x3(:, 3), 'k', 'LineWidth', 1.5);
title("Lorenz attractor with ro=1 (starting at 10, 10, 10)");

function dx = lorenz(t, x, params)
    dx = [
        params(1) * (x(2) - x(1));
        x(1) * (params(3) - x(3)) - x(2);
        x(1) * x(2) - params(2) * x(3);
    ];
end

function ind = error_diff(x1, x2)
    for i=(1:length(x1))
        error = sqrt(sum((x1(i, :) - x2(i, :)) .^ 2));
        if (error > 1)
            break;
        end
    end
    ind = i;
end