clear; close all; clc;

% alpha = 0.1
plot_phase(0.1, [1 -1], 'r.');

% alpha = 0.5
plot_phase(0.5, [1 .25], 'r.');

% alpha = 2
plot_phase( 2, [1 .5; 1 -.9; -1 0; -1 .9; 1 -.5], 'b.');

% alpha = 10
plot_phase(10, [1 .5; 1 -.9; -1 0; -1 .9; 1 -.5], 'b.');

function [] = plot_phase(alpha, inits, color)
    A = [alpha alpha; -.25 0];
    e = eig(A);
    disp("Eigenvalues");
    disp(e);
    [x, y] = meshgrid(-1:.1:1);
    figure;
    quiver(x, y, alpha * x + alpha * y, -0.25 * x);
    axis image;
    title("Phase plane and orbit for alpha: " + alpha);
    hold on
    for i = (1:size(inits, 1))
        x = inits(i, :);
        for j = (0:1:10000)
            plot(x(1), x(2), color);
            x = x - 0.01 * [alpha * x(1) + alpha * x(2), -0.25 * x(1)];
        end
    end
    hold off
end
