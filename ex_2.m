clear;
close all;

% x_prime = alpha - x^2
% x_node = +-sqrt(alpha)
figure;
axis image;
title('Saddle-node bifurcation (alpha - x^2)');
hold on
for alpha = (0:0.01:1)
    x_node_p = sqrt(alpha);
    x_node_n = -sqrt(alpha);
    plot(alpha, x_node_p, 'r.');
    plot(alpha, x_node_n, 'b.');
    xlim([-1 1]);
end
hold off

% x_prime = alpha - 2*x^2 - 2
% x_node = +-sqrt((alpha - 2)/2)
figure;
axis image;
title('Saddle-node bifurcation (alpha - 2*x^2 - 2)');
hold on
for alpha = (2:0.01:3)
    x_node_p = sqrt((alpha - 2) / 2);
    x_node_n = -sqrt((alpha - 2) / 2);
    plot(alpha, x_node_p, 'r.');
    plot(alpha, x_node_n, 'b.');
    xlim([1 3]);
end
hold off

% Are the systems topologically equivalent at alpha = 1?
% No
% Are the systems topologically equivalent at aplha = -1?
% Yes