clear;
close all;

% Part 1. Visualize at alpha = [-1.8, 0, 1.3];
for alpha=[-1.8, 0, 1.3]
    [x1, x2] = meshgrid(-2:.2:2);
    figure;
    quiver(x1, x2, x_prime_1(alpha, x1, x2), x_prime_2(alpha, x1, x2));
    axis image;
    title("alpha: " + alpha);
    hold on
    for i = (1:1:10)
        xn_prev = [4 * rand(1, 1) - 2, 4 * rand(1, 1) - 2];
        for x = (0:1:100)
            xn = xn_prev + 0.05 * [x_prime_1(alpha, xn_prev(1), xn_prev(2)), x_prime_2(alpha, xn_prev(1), xn_prev(2))];
            plot(xn(1), xn(2), 'r.');
            xn_prev = xn;
        end 
    end
    hold off
end


% Part 2. Visualize at alpha = 1; starting points (2,0) and (0.5, 0)

alpha = 1;
[x1, x2] = meshgrid(-2:.2:2);
figure;
quiver(x1, x2, x_prime_1(alpha, x1, x2), x_prime_2(alpha, x1, x2));
axis image;
title("alpha: " + alpha);
hold on
xn_prev = [2, 0];
for x = (0:1:100)
    xn = xn_prev + 0.05 * [x_prime_1(alpha, xn_prev(1), xn_prev(2)), x_prime_2(alpha, xn_prev(1), xn_prev(2))];
    plot(xn(1), xn(2), 'r.');
    xn_prev = xn;
end
xn_prev = [0.5, 0];
for x = (0:1:100)
    xn = xn_prev + 0.05 * [x_prime_1(alpha, xn_prev(1), xn_prev(2)), x_prime_2(alpha, xn_prev(1), xn_prev(2))];
    plot(xn(1), xn(2), 'b.');
    xn_prev = xn;
end 
hold off


% cusp bifurcation
figure;
title("Cusp bifurcation real roots");
hold on
for alpha1 = (-1:0.05:1)
    for alpha2 = (-1:0.05:1)
        p = [-1 0 alpha2 alpha1];
        r = roots(p);
        for i = 1:length(r)
            if imag(r(i)) == 0
                plot3(alpha1, alpha2, real(r(i)), 'b.');
                view(3);
            end
        end
    end
end
hold off


function x_prime = x_prime_1(alpha, x1, x2)
    x_prime = alpha * x1 - x2 - x1 * (x1^2 + x2^2);
end

function x_prime = x_prime_2(alpha, x1, x2)
    x_prime = x1 + alpha * x2 - x2 * (x1^2 + x2^2);
end