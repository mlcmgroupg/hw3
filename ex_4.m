clear;
close all;
clc;

% Q1) 0 < r < 2
logistic_map((0.01:0.01:2), "Bifurcation in Logistic map (0 < r < 2)");

% Q2) 2 < r < 4
logistic_map((2:0.01:4), "Bifurcation in Logistic map (2 < r <4)");

% Q3) 0 < r < 4
logistic_map((0.01:0.01:4), "Bifurcation in Logistic map (0 < r < 4)");


% Phase diagram
for r = [2.6 3.5 3.9 4]
    figure;
    xlim([0 1]);
    ylim([0 1]);
    title("Phase diagram for r=" + r);
    xlabel('Population (t)');
    ylabel('Population (t+1)');
    hold on
    for j = (0:1:1000)
        x = rand(1,1);
        for i = (0:1:1000)
            x = r * x * (1 - x);
        end
        x_next = r * x * (1 - x);
        plot(x, x_next, 'r.');
    end
    hold off
end

function [] = logistic_map(r, title_)
    figure;
    hold on
    for r = r
        x = 0.5;
        for t = (0:1:1000)
            x = r * x * (1 - x);
        end
        for t = (0:1:100)
            plot(r, x, 'r.');
            x = r * x * (1 - x);
        end
    end
    hold off
    title(title_);
    xlabel('r');
    ylabel('x');
end